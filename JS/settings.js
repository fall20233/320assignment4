/* eslint-disable no-return-assign */
let data;
let weatherData;
let page;

function addEvents() {
  page.city_input.addEventListener('change', () => {
    page.city = page.city_input.value;
    page.resetSelect();
    page.getAllCitiesInfo();
    // give the api time to fetch info
    setTimeout(() => {
      page.selectCountry();
    }, 600);
  });

  page.province_select.addEventListener('change', () => {
    page.getAllCitiesInfo();
    setTimeout(() => {
      page.updateLocationData();
    }, 500);
  });

  document.getElementById('getweather').addEventListener('click', (e) => {
    e.preventDefault();
    page.getWeatherInfo();
    setTimeout(() => {
      page.showData();
    }, 500);
  });
}
function resetInputs() {
  const inputs = document.querySelectorAll('input');
  inputs.forEach((elem) => {
    const e = elem;
    e.value = '';
  });
}

document.addEventListener('DOMContentLoaded', () => {
  resetInputs();
  page = {
    city_input: document.querySelector('#city_name_input'),
    province_select: document.querySelector('#country'),
    latitude: document.querySelector('#latitude'),
    longitude: document.querySelector('#longitude'),
    city: '',
    cityurl: '',
    weatherurl: '',
    getAllCitiesInfo() {
      if (this.province_select.value === 'null') {
        this.cityurl = `https://api.openweathermap.org/geo/1.0/direct?q=${this.city},,&limit=5&appid=ded957e6186d0ef7519776aa3d9a16a1`;
      } else {
        this.cityurl = `https://api.openweathermap.org/geo/1.0/direct?q=${this.city},${this.province_select.value},&limit=5&appid=ded957e6186d0ef7519776aa3d9a16a1`;
      }
      // eslint-disable-next-line no-unused-vars
      const lookup = fetch(this.cityurl)
        .then((resp) => resp.json())
        .then((result) => data = result)
        .catch((err) => console.log(err));
    },
    selectCountry() {
      data.forEach((elem) => {
        const opt = document.createElement('option');
        opt.value = elem.state;
        opt.textContent = elem.state;
        this.province_select.appendChild(opt);
      });
    },
    resetSelect() {
      this.province_select.innerHTML = '<option value="null"> ----- </option>';
    },
    updateLocationData() {
      if (this.province_select.value === 'null') {
        this.latitude.value = '';
        this.longitude.value = '';
      } else {
        this.latitude.value = data[0].lat;
        this.longitude.value = data[0].lon;
      }
    },
    getWeatherInfo() {
      this.weatherurl = `https://api.openweathermap.org/data/2.5/forecast?lat=${this.latitude.value}&lon=${this.longitude.value}&appid=ded957e6186d0ef7519776aa3d9a16a1&units=metric`;
      // eslint-disable-next-line no-undef
      lookup = fetch(this.weatherurl)
        .then((resp) => resp.json())
        .then((result) => weatherData = result)
        .catch((err) => console.log(err));
    },
    showData() {
      document.querySelector('#population').value = weatherData.city.population;

      // calculate sunrise time and put it in sunrise field
      const rise = weatherData.city.sunrise;
      const timeRise = new Date(rise * 1000);
      const riseHour = timeRise.getHours();
      const riseMinutes = timeRise.getMinutes();
      document.querySelector('#sunrise').value = `${riseHour}:${riseMinutes}`;

      // calculate sunset time and put it in sunset field
      const set = weatherData.city.sunset;
      const timeSet = new Date(set * 1000);
      const setHour = timeSet.getHours();
      const setMinutes = timeSet.getMinutes();
      document.querySelector('#sunset').value = `${setHour}:${setMinutes}`;
      this.htmlWeatherInfoDay();
      this.htmlWeatherInfoTomorrow();
      this.htmlWeatherInfoOvermorrow();
    },
    htmlWeatherInfoDay() {
      // get current time
      const currTime = weatherData.list[0].dt_txt;
      document.querySelector('#dayDate').textContent = currTime;
      // set current image
      const iconCode = weatherData.list[0].weather[0].icon;
      const imgUrl = `http://openweathermap.org/img/w/${iconCode}.png`;
      document.querySelector('#dayImg').innerHTML = `<img src="${imgUrl}">`;
      // set description
      const { description } = weatherData.list[0].weather[0];
      document.querySelector('#dayDescription').textContent = description;
      // set temperature
      const temperature = weatherData.list[0].main.temp;
      document.querySelector('#dayTemp').textContent = temperature;
      // set feels-like
      const feels = weatherData.list[0].main.feels_like;
      document.querySelector('#dayFeels').textContent = feels;
      // set humidity
      const { humidity } = weatherData.list[0].main;
      document.querySelector('#dayHumidity').textContent = `${humidity}%`;
      // set wind
      const wind = weatherData.list[0].wind.speed;
      document.querySelector('#dayWind').textContent = wind;
    },
    htmlWeatherInfoTomorrow() {
      // get current time
      const currTime = weatherData.list[8].dt_txt;
      document.querySelector('#tomorrowDate').textContent = currTime;
      // set current image
      const iconCode = weatherData.list[8].weather[0].icon;
      const imgUrl = `http://openweathermap.org/img/w/${iconCode}.png`;
      document.querySelector('#tomorrowImg').innerHTML = `<img src="${imgUrl}">`;
      // set description
      const { description } = weatherData.list[8].weather[0];
      document.querySelector('#tomorrowDescription').textContent = description;
      // set temperature
      const temperature = weatherData.list[8].main.temp;
      document.querySelector('#tomorrowTemp').textContent = temperature;
      // set feels-like
      const feels = weatherData.list[8].main.feels_like;
      document.querySelector('#tomorrowFeels').textContent = feels;
      // set humidity
      const { humidity } = weatherData.list[8].main;
      document.querySelector('#tomorrowHumidity').textContent = `${humidity}%`;
      // set wind
      const wind = weatherData.list[8].wind.speed;
      document.querySelector('#tomorrowWind').textContent = wind;
    },
    htmlWeatherInfoOvermorrow() {
      // get current time
      const currTime = weatherData.list[16].dt_txt;
      document.querySelector('#overmorrowDate').textContent = currTime;
      // set current image
      const iconCode = weatherData.list[16].weather[0].icon;
      const imgUrl = `http://openweathermap.org/img/w/${iconCode}.png`;
      document.querySelector('#overmorrowImg').innerHTML = `<img src="${imgUrl}">`;
      // set description
      const { description } = weatherData.list[16].weather[0];
      document.querySelector('#overmorrowDescription').textContent = description;
      // set temperature
      const temperature = weatherData.list[16].main.temp;
      document.querySelector('#overmorrowTemp').textContent = temperature;
      // set feels-like
      const feels = weatherData.list[16].main.feels_like;
      document.querySelector('#overmorrowFeels').textContent = feels;
      // set humidity
      const { humidity } = weatherData.list[16].main;
      document.querySelector('#overmorrowHumidity').textContent = `${humidity}%`;
      // set wind
      const wind = weatherData.list[16].wind.speed;
      document.querySelector('#overmorrowWind').textContent = wind;
    },
  };

  addEvents();
});
